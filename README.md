# Contenido

## Temas

- [Tema del manual](https://github.com/fniessen/org-html-themes)
- [Tema de lo hoja de evaluación](https://github.com/gongzhitaao/orgcss)
- [Tema org-minimal](https://github.com/caffo/org-minimal-html-theme.git)
- [Tema Org-Simple](https://www.btbytes.com/pages/org-simple.html)
- [Tema solarized](https://thomasf.github.io/solarized-css/)

# Compilación a Latex

Una vez creado el archivo tex se debe borrar la línea que crea la TOC manualmente

# Compilación a HTML

Es  productivo leer [CSS for  Org-exported HTML](https://gongzhitaao.org/orgcss/)

<del>Los  3  `</div>`  deben  ir  antes  de  la sección  de  bibliografía.   Se  ha  creado  [un
parche](html.patch) para eso.</del>

## Colocación

La página de evaluación debe ser el index.html de la versión web.

Copiar las carpetas `assets`, `im` y `styles` en el sitio.

La versión PDF debe ir en `assets/manual.pdf` y la versión HTML en `manual.html`.

# Bibliografía

Se usa ox-bibtex, ver https://vimeo.com/99167082


# Opinión

Es vergonzosa  la mediocridad de  la Facultad de  Ingeniería de la  UNAM. Ante esta situación, a
partir del semestre 2022-1 se ha adaptado la evaluación de tal forma que cuente solo un examen y
durante el semestre se obtendrán las evidencias necesarias para cumplir con la documentación que
exige la facultad.

El  examen debería  reducir notablemente  las posibilidades  de obtener  una mejor  calificación
debido a que está hecho para  cumplir con la expectativa del profesor, no para evaluar
el desempeño de los alumnos. Por esa razón se podrán hacer tres exámenes seguidos.

Lamentablemente, los  profesores,  ignorantes  en   su  mayoría,  solo  enseñan  temas  básicos,
prostituyen las  palabras de moda y  sonríen para evitar  el interrogatorio del alumno,  el cual
debería permitirle al mismo ver la mediocridad que el profesor busca ocultar.

Hoy en día, los  alumnos no buscan saber cosas y aprender la  profesión, su búsqueda consiste en
aprobar y conseguir el título sin saber nada,  con el menor esfuerzo. En casa solo han aprendido
a autocomplacerse y evitar cualquier momento que pueda ser incómodo. Se avecinan generaciones de
profesionales  que deberán  aprender fuera  de  la escuela,  en  medio del  trabajo, eso  traerá
múltiples incidentes que  pagará la sociedad que necesita y  solicita profesionistas capaces que
puedan resolver inteligentemente los problemas comunes.

La educación en la UNAM en estos momentos solo es un negocio. Aquellos que deseen romper ese negocio sostenido entre la complicidad de profesores mediocres y alumnos conformistas, son fuertemente insultados con la palabra ofensiva de moda, que en la actualidad también está relacionada con un acto criminal. Se trata de la única situación en la que se respaldan mutuamente; en cualquier otra ocasión, suelen competir para obtener el mayor beneficio que el negocio educativo pueda proporcionar.
