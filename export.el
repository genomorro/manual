;; General
(require 'package)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'htmlize)
(package-install 'citeproc)

;;; For the built-in themes which cannot use `require':
;; Add all your customizations prior to loading the themes
(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs nil
      modus-themes-region '(bg-only no-extend))

;; Load the theme of your choice:
(load-theme 'modus-operandi) ;; OR (load-theme 'modus-vivendi)

(require 'htmlize)

(setq org-html-htmlize-output-type 'inline-css)
(setq org-html-postamble t)
(setq org-html-postamble-format
      '(("en" "<p class=\"author\">Author: %a (%e)</p>
            <p class=\"date\">Last modification: %C</p>
            <p class=\"creator\">%c</p>")
	("es" "<p class=\"author\">Autor: %a (%e)</p>
            <p class=\"date\">Última modificación: %C</p>
            <p class=\"creator\">%c</p>")))
(setq org-footnote-auto-adjust t)
(setq org-html-validation-link nil)

(require 'citeproc)
(require 'oc-csl)

(setq org-cite-csl-styles-dir "./csl-styles/")
(setq org-cite-csl-locales-dir "./csl-locales/")

;; Lista del semestre

(defun derecho-a-examen-37 (a &optional e1 e2 e3)
  (if (or
       (<  a 80)
       (if  (or
             ( and e1 (numberp e1))
             (and  e1 (string= e1 "t"))
             ) nil t ))
       ;; (if (and
       ;;      e2 (> e 2 5)
       ;;      e3 (> e 3 5)
       ;;      ) nil t ))
       (format "NP")
       (format "Examen")))
(defun calificacion-37 (a &optional e1 e2 e3 ex f1 f2)
  (if (string= "Examen" (derecho-a-examen-37 a e1 e2 e3))
      (if (and f2 (> f2 0))
          (if (> f2 5)
              (format "%d" f2)
            (format "5"))
        (if (and f1 (> f1 0))
            (if (> f1 5)
                (format "%d" f1)
              (format "5"))
          (if (and ex (> ex 0))
              (if (> ex 5)
                  (format "%d" ex)
                (format "5"))
            (format "NP"))))
    (format "NP")))
(defun derecho-a-examen-25 (a &optional e1 e2 e3)
  (if (or
       (< a 80)
       ;; (if (or
       ;;      (and e1 (numberp e1))
       ;;      (and e1 (string= e1 "t"))
       ;;      ) nil t)
       (if (and
            e2 (> e2 5)
            ;; e3 (> e3 5)
            ) nil t)
       )
      (format "NP")
    (format "Examen")))
(defun calificacion-25 (a &optional e1 e2 e3 ex f1 f2)
  (if (string= "Examen" (derecho-a-examen-25 a e1 e2 e3))
      (if (and f2 (> f2 0))
          (if (> f2 5)
              (format "%d" f2)
            (format "5"))
        (if (and f1 (> f1 0))
            (if (> f1 5)
                (format "%d" f1)
              (format "5"))
          (if (and ex (> ex 0))
              (if (> ex 5)
                  (format "%d" ex)
                (format "5"))
            (format "NP"))))
    (format "NP")))

(defun dfeich/org-export-delete-commented-cols (back-end)
  "Delete columns $2 to $> marked as `<#>' on a row with `/' in $1.
  If you want a  non-empty column $1 to be deleted  make it $2 by
  inserting an empty column before and adding `/' in $1."
  (while (re-search-forward "^[ \t]*| +/ +|\\(.*|\\)? +\\(<#>\\) *|" nil t)
    (goto-char (match-beginning 2))
    (org-table-delete-column)
    (beginning-of-line)))
(add-hook 'org-export-before-processing-hook #'dfeich/org-export-delete-commented-cols)
